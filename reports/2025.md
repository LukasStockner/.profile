# 2025

<!--
Keep the order and titles unchanged, so this can be parsed automatically.
Uncomment as you create reports, so that the most recent entry is at the top.
-->

<!--

## December 22 - 28

## December 15 - 21

## December 8 - 14

## December 1 - 7

## November 24 - 30

## November 17 - 23

## November 10 - 16

## November 3 - 9

## October 27 - November 2

## October 20 - 26

## October 13 - 19

## October 6 - 12

## September 29 - October 5

## September 22 - 28

## September 15 - 21

## September 8 - 14

## September 1 - 7

## August 25 - 31

## August 18 - 24

## August 11 - 17

## August 4 - 10

## July 28 - August 3

## July 21 - 27

## July 14 - 20

## July 7 - 13

## June 30 - July 6

## June 23 - 29

## June 16 - 22

## June 9 - 15

## June 2 - 8

## May 26 - June 1

## May 19 - 25

## May 12 - 18

## May 5 - 11

## April 28 - May 4

## April 21 - 27

## April 14 - 20

## April 7 - 13

## March 31 - April 6

## March 24 - 30

## March 17 - 23

## March 10 - 16

## March 3 - 9

## February 24 - March 2

## February 17 - 23

## February 10 - 16

## February 3 - 9

-->

## January 27 - February 2

- Created and merged blender/blender!133689 (Cycles: Refactor: Store Cycles-specific pointers in our own ShaderGlobals)
- Created and merged blender/blender!133746 (Cycles: Bump minimum OSL version to 1.13.4)
- Merged blender/blender!133669 (Fix blender/blender#131218: Cycles: Race condition when syncing motion blur)
- Created and merged blender/blender!133788 (Fix: Cycles: Wrong motion transform interpolation when using velocity)
  - Committed blender/blender-test-data@3eca8bc3318e5f0ce9f0b53e9c075901cc7a52a0 (Cycles: Add a render test for velocity multi-step motion blur)
- Reviewed blender/blender!133848 (Fix: cycles MaterialX subsurface calibration)
- Reviewed blender/blender!133845 (Cycles: Burley diffuse closure for MaterialX compatibility)
- Updated blender/blender!131517 (Support setting UI Name for custom properties)
- Updated blender/blender!129495 (WIP: Cycles: Support for custom OSL cameras)
  - Now split into logical commits for easier review

## January 20 - 26

- Committed blender/blender@2e12abcc71f3e35f7af9cdefe087e9f89e2aa1c8 (Cycles: Fix compilation without OSL)
- Reviewed blender/blender!132912 (Fix #128733: Cycles crash with host memory fallback and interactive updates)
- Reviewed blender/blender!132717 (Cleanup: Compositor: Ghost data of a node storage)
- Reviewed blender/blender!133245 (Fix #133229: Remove camera size optimization from SSS shader in Cycles)
- Looked into blender/blender#133254 (Eyedropper doesn't function as expected on Empty Image Planes when in Viewport Rendered)
- Fixed blender/blender#108215 and blender/blender#132535 (Cycles: Point Cloud uses wrong position with Persistent Data)
- Fixed blender/blender#132516 (Cycles: No NDC coordinates with OSL on OptiX)
- Looked into blender/blender#131218 (Cycles: Crash while rendering animation using motion blur and persistent data)
- Spent a lot of time looking into Cycles motion blur syncing for the crash above
  - Turns out the velocity attribute support broke some assumptions here
  - Created blender/blender!133669 (Cycles: Race condition when syncing motion blur)
  - Found additional bug about mismatch between object transform's and geometry's motion blur steps
    - PR incoming soon
  - Found plenty of deduplication opportunities in motion blur geometry code between a) multiple device backends and b) multiple geometry types
    - PRs incoming soon
- Reviewed blender/blender!133597 (Cycles: OSL oren_nayar_diffuse_bsdf compatibility for MaterialX)
- Started working on OSL ShaderGlobals cleanup, PR incoming soon


## January 13 - 19

See above.

## January 6 - 12

- Enjoyed the holidays
- Continued working on OSL camera PR (blender/blender!129495)
  - Implemented attributes to be able to query common parameters from shaders
  - Identified and fixed [upstream OSL bug causing issues with the derivatives](https://github.com/AcademySoftwareFoundation/OpenShadingLanguage/pull/1916)
  - Started to break the PR into individually reviewable chunks
  - Noticed big chunk of cleanup/refactor/lint commits to the Cycles code that caused conflicts all over the place
  - Started from scratch, rebased current PR onto latest `main`
  - Updated PR with rebased state
  - Started the PR breakup once again
- Reviewed blender/blender!132067 (Cleanup: replace floatX_to_floatY() with make_floatY())
- Started looking into replacing Blender OpenEXR loading with OIIO to implement multipart EXR, will be very non-trivial

## December 30 - January 5
See above.